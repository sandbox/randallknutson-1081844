<?php
// $Id:$

/**
 * @file
 * Base rate shipping method administration menu items.
 *
 */

/**
 * List and compare all basepercentrate shipping quote methods.
 */
function uc_basepercentrate_admin_methods() {
  $output = '';

  $rows = array();
  $enabled = variable_get('uc_quote_enabled', array());
  $weight = variable_get('uc_quote_method_weight', array());
  $result = db_query("SELECT mid, title, label, base_rate, percent_rate FROM {uc_basepercentrate_methods}");
  while ($method = db_fetch_object($result)) {
    $row = array();
    $row[] = check_plain($method->title);
    $row[] = check_plain($method->label);
    $row[] = uc_currency_format($method->base_rate);
    $row[] = $method->percent_rate . '%';
    $row[] = l(t('edit'), 'admin/store/settings/quotes/methods/basepercentrate/'. $method->mid);
    $row[] = l(t('conditions'), CA_UI_PATH .'/uc_basepercentrate_get_quote_'. $method->mid .'/edit/conditions');
    $rows[] = $row;
  }
  if (count($rows)) {
    $header = array(t('Title'), t('Label'), t('Base rate'), t('Default product rate'), t('Free rate above'), array('data' => t('Operations'), 'colspan' => 2));
    $output .= theme('table', $header, $rows);
  }
  $output .= l(t('Add a new minimum rate shipping method.'), 'admin/store/settings/quotes/methods/basepercentrate/add');
  return $output;
}

/**
 * Configure the store default product shipping rates.
 */
function uc_basepercentrate_admin_method_edit_form($form_state, $mid = 0) {
  $form = array();
  $sign_flag = variable_get('uc_sign_after_amount', FALSE);
  $currency_sign = variable_get('uc_currency_sign', '$');

  if (is_numeric($mid) && ($method = db_fetch_object(db_query("SELECT * FROM {uc_basepercentrate_methods} WHERE mid = %d", $mid)))) {
    $form['mid'] = array('#type' => 'value', '#value' => $mid);
  }
  $form['title'] = array('#type' => 'textfield',
    '#title' => t('Shipping method title'),
    '#description' => t('The name shown to distinguish it from other basepercentrate methods.'),
    '#default_value' => $method->title,
  );
  $form['label'] = array('#type' => 'textfield',
    '#title' => t('Line item label'),
    '#description' => t('The name shown to the customer when they choose a shipping method at checkout.'),
    '#default_value' => $method->label,
  );
  $form['base_rate'] = array('#type' => 'textfield',
    '#title' => t('Base rate'),
    '#description' => t('The base rate that the percentage will be added to.'),
    '#default_value' => $method->base_rate,
    '#size' => 16,
    '#field_prefix' => $sign_flag ? '' : $currency_sign,
    '#field_suffix' => $sign_flag ? $currency_sign : '',
  );
  $form['percent_rate'] = array('#type' => 'textfield',
    '#title' => t('Default product percent shipping rate'),
    '#default_value' => $method->percent_rate,
    '#size' => 16,
    '#field_suffix' => '%',
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['buttons']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('uc_basepercentrate_admin_method_edit_form_delete'),
  );

  return $form;
}

function uc_basepercentrate_admin_method_edit_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Submit')) {
    if (!empty($form_state['values']['base_rate']) && !is_numeric($form_state['values']['base_rate'])) {
      form_set_error('base_rate', t('The base rate must be a numeric amount.'));
    }
    if (!empty($form_state['values']['percent_rate']) && !is_numeric($form_state['values']['percent_rate'])) {
      form_set_error('percent_rate', t('The product rate must be a numeric amount.'));
    }
  }
}

function uc_basepercentrate_admin_method_edit_form_delete($form, &$form_state) {
  drupal_goto('admin/store/settings/quotes/basepercentrate/'. $form_state['values']['mid'] .'/delete');
}

function uc_basepercentrate_admin_method_edit_form_submit($form, &$form_state) {
  if ($form_state['values']['mid']) {
    db_query("UPDATE {uc_basepercentrate_methods} SET title = '%s', label = '%s', base_rate = %f, percent_rate = %f WHERE mid = %d",
      $form_state['values']['title'], $form_state['values']['label'], $form_state['values']['base_rate'], $form_state['values']['percent_rate'], $form_state['values']['mid']);
    drupal_set_message(t("Minimum rate shipping method was updated."));
  }
  else {
    db_query("INSERT INTO {uc_basepercentrate_methods} (title, label, base_rate, percent_rate) VALUES ('%s', '%s', %f, %f)",
      $form_state['values']['title'], $form_state['values']['label'], $form_state['values']['base_rate'], $form_state['values']['percent_rate']);
    $mid = db_last_insert_id('uc_basepercentrate_methods', 'mid');
    $enabled = variable_get('uc_quote_enabled', array());
    $enabled['basepercentrate_'. $mid] = TRUE;
    variable_set('uc_quote_enabled', $enabled);
    $weight = variable_get('uc_quote_method_weight', array());
    $weight['basepercentrate_'. $mid] = 0;
    variable_set('uc_quote_method_weight', $weight);
    drupal_set_message(t("Created and enabled new minimum rate shipping method."));
  }
  $form_state['redirect'] = 'admin/store/settings/quotes/methods/basepercentrate';
}

/******************************************************************************
 * Menu Callbacks                                                             *
 ******************************************************************************/

function uc_basepercentrate_admin_method_confirm_delete($form_state, $mid) {
  $form = array();
  $form['mid'] = array('#type' => 'value', '#value' => $mid);

  return confirm_form($form, t('Do you want to delete this shipping method?'),
    'admin/store/settings/quotes/methods/basepercentrate',
    t('This will remove the shipping method, Conditional Action predicate, and the
      product-specific overrides (if applicable). This action can not be undone.'),
    t('Delete'));
}

function uc_basepercentrate_admin_method_confirm_delete_submit($form, &$form_state) {
  db_query("DELETE FROM {uc_basepercentrate_methods} WHERE mid = %d", $form_state['values']['mid']);
  db_query("DELETE FROM {uc_basepercentrate_products} WHERE mid = %d", $form_state['values']['mid']);
  ca_delete_predicate('uc_basepercentrate_get_quote_'. $form_state['values']['mid']);

  $enabled = variable_get('uc_quote_enabled', array());
  unset($enabled['basepercentrate_'. $form_state['values']['mid']]);
  variable_set('uc_quote_enabled', $enabled);

  $weight = variable_get('uc_quote_method_weight', array());
  unset($weight['basepercentrate_'. $form_state['values']['mid']]);
  variable_set('uc_quote_method_weight', $weight);

  drupal_set_message(t('Minimum rate shipping method deleted.'));
  $form_state['redirect'] = 'admin/store/settings/quotes/methods/basepercentrate';
}

